package ua.shpp.command_classes;

import ua.shpp.abstracts.Command;
import ua.shpp.classes.Lift;

public class InvokeCommand implements Command {
    Lift lift;

    public InvokeCommand(Lift lift) {
        this.lift = lift;
    }

    @Override
    public void execute() {
        lift.invoke();
    }
}
