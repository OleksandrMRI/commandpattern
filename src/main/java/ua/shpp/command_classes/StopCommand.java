package ua.shpp.command_classes;

import ua.shpp.abstracts.Command;
import ua.shpp.classes.Lift;

public class StopCommand implements Command {
    Lift lift;
    public StopCommand(Lift lift) {
        this.lift = lift;
    }
    @Override
    public void execute() {
        lift.stop();
    }
}
