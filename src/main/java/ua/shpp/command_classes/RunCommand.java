package ua.shpp.command_classes;

import ua.shpp.abstracts.Command;
import ua.shpp.classes.Lift;

public class RunCommand implements Command {
    Lift lift;
    int floor;

    public RunCommand(Lift lift, int floor) {
        this.lift = lift;
        this.floor = floor;
    }

    @Override
    public void execute() {
        lift.runToTheFloor(floor);
    }

}
