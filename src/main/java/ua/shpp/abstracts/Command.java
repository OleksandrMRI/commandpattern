package ua.shpp.abstracts;

public interface Command {
    void execute();
}
