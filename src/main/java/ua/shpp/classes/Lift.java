package ua.shpp.classes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Lift {
    Logger log = LoggerFactory.getLogger(Lift.class);

    public void invoke() {
        log.info("Lift runs to passenger\n");
    }

    public void stop() {
        log.info("Lift stops emergency\n");
    }

    public void runToTheFloor(int floor) {
        log.info("Lift with passenger runs to {} floor\n", floor);
    }
}

