package ua.shpp.classes;

import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.abstracts.Command;
import ua.shpp.command_classes.*;

public class CommandBox {
    ImmutableMap<Commands, Command> immutableMap;
    Command unnounCommand;
    Logger log = LoggerFactory.getLogger(CommandBox.class);
    public CommandBox(Lift lift) {
        unnounCommand=new UnknownCommand();
        immutableMap= ImmutableMap.<Commands,Command>builder()
                .put(Commands.STOP,new StopCommand(lift))
                .put(Commands.INVOKE,new InvokeCommand(lift))
                .put(Commands.RUN1,new RunCommand(lift,1))
                .put(Commands.RUN2,new RunCommand(lift,2))
                .put(Commands.RUN3,new RunCommand(lift,3))
                .put(Commands.RUN4,new RunCommand(lift,4))
                .put(Commands.RUN5,new RunCommand(lift,5))
                .build();
    }
    public Command getCommand(Commands commands){
        log.warn("Command is: {}", commands);
        return immutableMap.getOrDefault(commands, unnounCommand);
    }
}
