package ua.shpp.classes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.abstracts.Command;

public class UnknownCommand implements Command {
    Logger log = LoggerFactory.getLogger(UnknownCommand.class);
    @Override
    public void execute() {
        log.info("I`ll be back!");
    }
}
