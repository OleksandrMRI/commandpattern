package ua.shpp.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.classes.*;

import java.util.Scanner;

public class AppCommand {
    private static final Logger log = LoggerFactory.getLogger(AppCommand.class);
    public static void main(String[] args) {
        useLift();
    }

    private static void useLift() {
        Lift lift = new Lift();
        CommandBox commandBox = new CommandBox(lift);
        Passenger passenger = new Passenger();
        Scanner scanner = new Scanner(System.in);
        boolean liftUsed = true;
        for (int i = 0; i < 5; i++) {

            log.warn("Enter command request: ");
            String request = scanner.nextLine().toUpperCase();
            if(request.equals("SLEEP")){
                liftUsed = false;
            }
            commandBox.getCommand(passenger.action(/*request*/)).execute();
        }
        log.info("Вася валимо нас спалили!");

    }
}